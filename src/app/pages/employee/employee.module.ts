import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EmployeeRoutingModule, routedComponents } from './employee-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    EmployeeRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class EmployeeModule { }
