import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { OtherRoutingModule, routedComponents } from './other-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    OtherRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class OtherModule { }
