import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherComponent } from './other.component';
import { Submenu1Component } from './submenu1/submenu1.component';
import { Submenu2Component } from './submenu2/submenu2.component';

const routes: Routes = [{
  path: '',
  component: OtherComponent,
  children: [{
    path: 'login',
    component: Submenu1Component,
  }, {
    path: 'submenu2',
    component: Submenu2Component,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class OtherRoutingModule {

}

export const routedComponents = [
  OtherComponent,
    Submenu1Component,
    Submenu2Component,
];
