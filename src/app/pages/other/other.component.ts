import { Component } from '@angular/core';

@Component({
  selector: 'ngx-other-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class OtherComponent {
}
