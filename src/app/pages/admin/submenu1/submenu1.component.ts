import { Component } from '@angular/core';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./submenu1.component.scss'],
  templateUrl: './submenu1.component.html',
})
export class Submenu1Component {

  starRate = 2;
  heartRate = 4;
}
