import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, /*{
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  },*/ /*{
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  },*/ /*{
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  },*/
      /*{
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, */
      /*{
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  },*/
  {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'menu1',
    loadChildren: './menu1/menu1.module#Menu1Module',
  }, {
    path: 'other',
    loadChildren: './other/other.module#OtherModule',
  }, {
    path: 'employee',
    loadChildren: './employee/employee.module#EmployeeModule',
  }, {
    path: 'manager',
    loadChildren: './manager/menu1.module#Menu1Module',
  }, {
    path: 'admin',
    loadChildren: './admin/menu1.module#Menu1Module',
  }, {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
