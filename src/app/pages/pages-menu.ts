import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'other',
        icon: 'ion-help',
        children: [
            {
                title: 'login',
                link: '/pages/other/login',
            },
            /*{
                title: 'login2',
                link: '/login',
            },*/
            {
                title: 'เข้าสู่ระบบครั้งแรก',
                link: '/pages/other/submenu2',
            },
        ],
    },
    {
        title: 'Employee',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'ข้อมูลส่วนตัว',
                link: '/pages/employee/proflie',
            },
            {
                title: 'ลงเวลาทำงาน',
                link: '/pages/employee/downtime',
            },
            {
                title: 'ลงคนแทนงาน',
                link: '/pages/employee/delegate',
            },
            {
                title: 'ออกรายงาน',
                link: '/pages/employee/report',
            },
        ],
    },
    {
        title: 'manager',
        icon: 'nb-compose',
        children: [
            {
                title: 'submenu1',
                link: '/pages/manager/submenu1',
            },
            {
                title: 'submenu2',
                link: '/pages/manager/submenu2',
            },
        ],
    },
    {
        title: 'admin',
        icon: 'nb-compose',
        children: [
            {
                title: 'submenu1',
                link: '/pages/admin/submenu1',
            },
            {
                title: 'submenu2',
                link: '/pages/admin/submenu2',
            },
        ],
    },
  {
    title: 'Forms',
    icon: 'nb-compose',
    children: [
      {
        title: 'Form Inputs',
        link: '/pages/forms/inputs',
      },
      {
        title: 'Form Layouts',
        link: '/pages/forms/layouts',
      },
    ],
  },
    {
        title: 'menu1',
        icon: 'nb-compose',
        children: [
            {
                title: 'submenu1',
                link: '/pages/menu1/submenu1',
            },
            {
                title: 'submenu2',
                link: '/pages/menu1/submenu2',
            },
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/login',
        home: true,
    },
  /*{
    title: 'Components',
    icon: 'nb-gear',
    children: [
      {
        title: 'Tree',
        link: '/pages/components/tree',
      }, {
        title: 'Notifications',
        link: '/pages/components/notifications',
      },
    ],
  },*/
  /*{
    title: 'Miscellaneous',
    icon: 'nb-shuffle',
    children: [
      {
        title: '404',
        link: '/pages/miscellaneous/404',
      },
    ],
  },*/
  /*{
    title: 'Auth',
    icon: 'nb-locked',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },*/
];

const menuDown = add();
function add() {
    const menuDown = [];
    let user_status = localStorage.getItem('user_status');
    switch(user_status) {
        case '1': {
            menuDown = [
                {
                    title: 'Dashboard',
                    icon: 'nb-home',
                    link: '/pages/dashboard',
                    home: true,
                },
                {
                    title: 'FEATURES',
                    group: true,
                },
                {
                    title: 'Employee',
                    icon: 'ion-arrow-down-b',
                    children: [
                        {
                            title: 'ข้อมูลส่วนตัว',
                            link: '/pages/employee/proflie',
                        },
                        {
                            title: 'ลงเวลาทำงาน',
                            link: '/pages/employee/downtime',
                        },
                        {
                            title: 'ลงคนแทนงาน',
                            link: '/pages/employee/delegate',
                        },
                        {
                            title: 'ออกรายงาน',
                            link: '/pages/employee/report',
                        },
                    ],
                },
                {
                    title: 'logOut',
                    icon: 'ion-log-out',
                    link: '/login',
                },
            ];
            break;
        }
        case '2': {
            menuDown = [
                {
                    title: 'Dashboard',
                    icon: 'nb-home',
                    link: '/pages/dashboard',
                    home: true,
                },
                {
                    title: 'FEATURES',
                    group: true,
                },
                {
                    title: 'Employee',
                    icon: 'ion-arrow-down-b',
                    children: [
                        {
                            title: 'ข้อมูลส่วนตัว',
                            link: '/pages/employee/proflie',
                        },
                        {
                            title: 'ลงเวลาทำงาน',
                            link: '/pages/employee/downtime',
                        },
                        {
                            title: 'ลงคนแทนงาน',
                            link: '/pages/employee/delegate',
                        },
                        {
                            title: 'ออกรายงาน',
                            link: '/pages/employee/report',
                        },
                    ],
                },
                {
                    title: 'logOut',
                    icon: 'ion-log-out',
                    link: '/login',
                },
            ];
            break;
        }
        case '3': {
            menuDown = [
                {
                    title: 'Dashboard',
                    icon: 'nb-home',
                    link: '/pages/dashboard',
                    home: true,
                },
                {
                    title: 'FEATURES',
                    group: true,
                },
                {
                    title: 'manager',
                    icon: 'ion-arrow-down-b',
                    children: [
                        {
                            title: 'รายชื่อพนักงาน',
                            link: '/pages/manager/submenu1',
                        },
                        {
                            title: 'ให้ดาว',
                            link: '/pages/manager/submenu2',
                        },
                        {
                            title: 'ตั่งค่าแทค',
                            link: '/pages/manager/submenu3',
                        },
                        {
                            title: 'นำเข้าข้อมูล',
                            link: '/pages/manager/submenu4',
                        },
                        {
                            title: 'Report',
                            link: '/pages/manager/submenu5',
                        },
                    ],
                },
                {
                    title: 'logOut',
                    icon: 'ion-log-out',
                    link: '/login',
                },
            ];
            break;
        }
        case '4': {
            menuDown = [
                {
                    title: 'Dashboard',
                    icon: 'nb-home',
                    link: '/pages/dashboard',
                    home: true,
                },
                {
                    title: 'FEATURES',
                    group: true,
                },
                {
                    title: 'admin',
                    icon: 'ion-arrow-down-b',
                    children: [
                        {
                            title: 'submenu1',
                            link: '/pages/admin/submenu1',
                        },
                        {
                            title: 'submenu2',
                            link: '/pages/admin/submenu2',
                        },
                    ],
                },
                {
                    title: 'logOut',
                    icon: 'ion-log-out',
                    link: '/login',
                },
            ];
            break;
        }
        default: {
            menuDown = [];
            break;
        }
    }

    return menuDown;
}


export const MENU_EMP: NbMenuItem[] = menuDown;
