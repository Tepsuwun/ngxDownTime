import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-submenu4',
  styleUrls: ['./submenu4.component.scss'],
  templateUrl: './submenu4.component.html',
})
export class Submenu4Component implements OnInit {
    allFin = [];

    constructor(private Auth: AuthService,
                private router: Router) { }
    ngOnInit() {
        this.Auth.getuploadFin().subscribe(data => {
            // console.log (data.data);
            this.allFin = data.data;
        })
    }

}
