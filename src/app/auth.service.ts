import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface myData {
    success: boolean,
    message: string,
    data: Object,
    user_status: string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false

  constructor(private http: HttpClient) { }

    setLoggedIn(value: boolean) {
        this.loggedInStatus = value
    }

    get isLoggedIn() {
        return this.loggedInStatus
    }

    getUserDetails(username, password) {
        // return this.http.post<myData>('http://localhost:1234/api/login.php', {
        return this.http.post<myData>('http://localhost/test/api/login.php', {
            username,
            password
        })
    }

    getTest() {
        // return this.http.post<myData>('http://localhost:1234/api/jsonArray.php', {
        return this.http.post<myData>('http://localhost/test/api/jsonArray.php', {
        })
    }

    getuploadFin() {
        // return this.http.post<myData>('http://localhost:1234/api/jsonArray.php', {
        return this.http.post<myData>('http://localhost/test/txt/uploadFin.php', {
        })
    }
}
