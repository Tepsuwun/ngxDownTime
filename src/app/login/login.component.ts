import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    alluser = [];
  constructor(private Auth: AuthService,
              private router: Router) { }
  ngOnInit() {
      localStorage.clear();
      this.Auth.getTest().subscribe(data => {
          // console.log (data.data);
          localStorage.setItem('data', JSON.stringify(data.data));
          this.alluser = data.data;
          localStorage.setItem('jsonArray', data.message);
      })
  }

    loginUser(event) {
        event.preventDefault()
        const target = event.target
        const username = target.querySelector('#username').value
        const password = target.querySelector('#password').value

        this.Auth.getUserDetails(username, password).subscribe(data => {
            if (data.success) {
                // localStorage.clear();
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
                // window.alert(data.message)
                this.router.navigate([''])
                this.Auth.setLoggedIn(true)
            } else {
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
                // window.alert(data.message)
            }
            localStorage.setItem('user_status', data.user_status);
            localStorage.setItem('loginSuccess', data.success);
            let myItem = localStorage.getItem('loginSuccess');
            // console.log (myItem);
            console.log ('loginSuccess => ', myItem);
        })
        console.log(username, password)
    }

}
