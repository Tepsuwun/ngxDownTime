import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // return true;
      let myItem = localStorage.getItem('loginSuccess');
      if (myItem == 'true') {
          console.log ('loginSuccess => ', myItem , '(เข้าสู่ระบบสำเร็จ)');
          return true
      }else {
          console.log ('loginSuccess => ', myItem , '(เข้าสู่ระบบไม่สำเร็จ)');
          localStorage.clear();
          this.router.navigate(['/login'])
          return false
      }
      // return this.auth.isLoggedIn;
  }
}
